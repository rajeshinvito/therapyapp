//
//  AppDelegate.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/24/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.setupIQKeyboardManager()
        self.navigateToPage()
        return true
    }
    
    
}
//MARK:- func delgates
extension AppDelegate {
    func setupIQKeyboardManager(){
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.resignFirstResponder()
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    }
    
    func navigateToPage(){
        
        let login = UserDefaults.standard.bool(forKey: "LoggedIn")
        var navigation = UINavigationController()
        let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if login {
            let initialViewControlleripad : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "Dashboardvc") as! Dashboardvc
            self.window = UIWindow(frame: UIScreen.main.bounds)
            navigation = UINavigationController(rootViewController: initialViewControlleripad)
        }else{
            let initialViewControlleripad : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.window = UIWindow(frame: UIScreen.main.bounds)
            navigation = UINavigationController(rootViewController: initialViewControlleripad)
        }
        self.window?.rootViewController = navigation
        self.window?.makeKeyAndVisible()
        
    }
    func applicationWillResignActive(_ application: UIApplication) {
       
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
       
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
//        var start_time = 0
//        SimpleTimer(interval: 3,repeats: true){
//            start_time =  UserDefaults.standard.integer(forKey: "start_time")
//            start_time += 1
//            UserDefaults.standard.set(start_time, forKey: "start_time")
//        }.start()
    }
    
}
