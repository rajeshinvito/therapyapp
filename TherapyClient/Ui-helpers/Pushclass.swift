//
//  Pushclass.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/24/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func movetonextvc(id:String,storyBordid : String){
        let Storyboard : UIStoryboard = UIStoryboard(name: storyBordid, bundle: nil)
        let nxtVC = Storyboard.instantiateViewController(withIdentifier: id)
        self.navigationController?.pushViewController(nxtVC, animated: true)
    }
    func popToBackVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func uiAnimate(view : NSLayoutConstraint, Constratint : Float){
        UIView.animate(withDuration:0.2, delay: 0.1, options: UIView.AnimationOptions.curveEaseIn, animations: {
            view.constant = CGFloat(Constratint)
              self.view.layoutIfNeeded()
          }, completion: nil)
    }
  
}



