//
//  timer.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/26/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import UIKit


class Simple_Timer {
    
    var seconds = 0
    var timer = Timer()
    var isTimerRunning = false
    var resumeTapped = false
    
    init(seconds : Int ,timer : Timer,isTimerRunning : Bool , resumeTapped : Bool) {
        self.seconds = seconds
        self.timer = timer
        self.isTimerRunning = isTimerRunning
        self.resumeTapped = resumeTapped
    }
    
    func hmsFrom(seconds: Int, completion: @escaping (_ hours: Int, _ minutes: Int, _ seconds: Int)->()) {
        
        completion(seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
        
    }
    func getStringFrom(seconds: Int) -> String {
        
        return seconds < 10 ? "0\(seconds)" : "\(seconds)"
    }
    
    func runTimer(){
         timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(Simple_Timer.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        seconds += 1
        self.hmsFrom(seconds: seconds) { hours, minutes, seconds in
            let hours = self.getStringFrom(seconds: hours)
            let minutes = self.getStringFrom(seconds: minutes)
            let seconds = self.getStringFrom(seconds: seconds)
           // print("\(hours):\(minutes):\(seconds)")
            if hours == "00" {
                if minutes == "00" {
                    if seconds != "00" {
                        UserDefaults.standard.set("\(seconds) seconds", forKey: "start_time")
                    }
                }else {
                    UserDefaults.standard.set("\(minutes) Min \(seconds) seconds", forKey: "start_time")
                }
            }else {
                UserDefaults.standard.set("\(hours) Hours \(minutes) Min \(seconds) seconds", forKey: "start_time")
            }
            
            UserDefaults.standard.set(seconds, forKey: "start_sec")
             UserDefaults.standard.set("\(hours):\(minutes):\(seconds)", forKey: "start_in")
        }
    }
    
   
    
    func pousetimer(){
        if self.resumeTapped == false {
            timer.invalidate()
            self.resumeTapped = true
        } else {
            runTimer()
            self.resumeTapped = false
        }
        UserDefaults.standard.set(0, forKey: "start_time")
    }
}


extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
