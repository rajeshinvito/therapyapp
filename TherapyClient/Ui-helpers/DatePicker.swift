//
//  DatePicker.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/24/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
 
    func showDatePicker(inputpicker: UIDatePicker){
         let inputpicker = UIDatePicker()
    //Formate Date
    inputpicker.datePickerMode = .date

   //ToolBar
   let toolbar = UIToolbar();
   toolbar.sizeToFit()
   let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
  let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));

 toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)

 // input_tf.inputAccessoryView = toolbar
 // input_tf.inputView = inputpicker

 }

    @objc func donedatePicker(input_tf : UILabel,input_tf2 : UILabel ,inputpicker: UIDatePicker){

   let formatter = DateFormatter()
   formatter.dateFormat = "dd/MM/yyyy"
   input_tf.text = formatter.string(from: inputpicker.date)
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "HH:mm a"
    input_tf2.text = formatter2.string(from: inputpicker.date)
   self.view.endEditing(true)
 }

 @objc func cancelDatePicker(){
    self.view.endEditing(true)
  }
}



//
//extension WorkHistoryVC: UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UNUserNotificationCenterDelegate {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        <#code#>
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        <#code#>
//    }
//    
////: UIPickerViewDelegate,UIPickerViewDataSource{
//   
////    func showpicker(){
////        picker = UIPickerView.init()
////        picker.delegate = self
////        picker.backgroundColor = UIColor.white
////        picker.setValue(UIColor.black, forKey: "textColor")
////        picker.autoresizingMask = .flexibleWidth
////        picker.contentMode = .center
////        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
////        self.view.addSubview(picker)
////        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
////        toolBar.barStyle = .blackTranslucent
////        toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]
////        self.view.addSubview(toolBar)
////    }
////    @objc func onDoneButtonTapped() {
////        toolBar.removeFromSuperview()
////        picker.removeFromSuperview()
////    }
////    func numberOfComponents(in pickerView: UIPickerView) -> Int {
////        return 1
////    }
////
////    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
////        return YOUR_DATA_ARRAY.COUNT
////    }
////
////    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
////        print(YOUR_DATA_ARRAY[row])
////    }
////
////    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
////        print(YOUR_DATA_ARRAY[row])
////    }
//    
//  func doDatePicker(){
//           // DatePicker
//           self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 200))
//           self.datePicker?.backgroundColor = UIColor.white
//    self.datePicker?.datePickerMode = UIDatePicker.Mode.dateAndTime
//           datePicker.center = view.center
//           view.addSubview(self.datePicker)
//
//           // ToolBar
//
//           toolBar.barStyle = .default
//           toolBar.isTranslucent = true
//           toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
//           toolBar.sizeToFit()
//
//           // Adding Button ToolBar
//           let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(HVACViewController.doneClick))
//           let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
//           let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(HVACViewController.cancelClick))
//           toolBar.setItems([cancelButton, spaceButton, doneButton], animated: true)
//           toolBar.isUserInteractionEnabled = true
//
//           self.view.addSubview(toolBar)
//           self.toolBar.isHidden = false
//
//
//       }
//
//
//       func doneClick() {
//           let dateFormatter1 = DateFormatter()
//           dateFormatter1.dateStyle = .medium
//           dateFormatter1.timeStyle = .none
//          // setNotification()
//           //self.datePicker.resignFirstResponder()
//           datePicker.isHidden = true
//           self.toolBar.isHidden = true
//
//
//       }
//
//       func cancelClick() {
//           datePicker.isHidden = true
//           self.toolBar.isHidden = true
//
//       }
//}
