//
//  apiManager.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/26/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

enum DataNotFound:Error {
    case dataNotFound
}

typealias successs = (Data)-> Void
typealias failure = (Any)-> Void
typealias Response = [String:Any]
var AccessToken = String()


func header()->HTTPHeaders{
    let bearerToken = UserDefaults.standard.string(forKey:"Accesstoken") ?? ""
    var headerMessage :HTTPHeaders!
    if bearerToken != ""{
        headerMessage = [
            "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization":"Bearer \(bearerToken)"]
        
    }else{
        headerMessage = [
            "Content-Type":"application/x-www-form-urlencoded","Accept":"application/json" ]
    }
    return headerMessage
}

class NetworkManager{
    var alamoFireManager : SessionManager?
    private init(){}
    
    static  var alamoFireManager : SessionManager?
    
    
    
    static func Apicalling<T:Mappable>(
        url:String,
        paramaters:Response = [:],
        httpMethodType:HTTPMethod = .post ,
        success:@escaping(T)-> () ,
        failure: @escaping failure)
    {
        if Connectivity.isNotConnectedToInternet{
            failure(ATErrorMessage.Server.checkConnection)
        }
        guard let httpMethod = HTTPMethod(rawValue: httpMethodType.rawValue) else{
            assertionFailure("OOPS !!! HTTP Method Not Found \(httpMethodType)")
            return
        }
        print(" ---------- T ** Model  ---------- \n ","\(T.self)")
        print(" ---------- HTTP Method  ---------- \n",httpMethod)
        print(" ----------  URL  ---------- \n",url)
        print(" ---------- parameters  ---------- \n",paramaters)
        print(" ---------- Header  ---------- \n",header())
        
        let configuration = URLSessionConfiguration.default
        alamoFireManager = Alamofire.SessionManager(configuration: configuration)
        alamoFireManager?.request(url, method: httpMethod, parameters: paramaters, encoding:  URLEncoding.default, headers: header()).responseJSON { (response) in
            print(response.response?.statusCode as Any)
            switch response.result{
            case .success(let value):
                guard let responseDict = value as? Response else{return}
                print(" ---------- responseDict  ---------- \n",responseDict)
                guard let item = Mapper<T>.init().map(JSON: responseDict) else{
                    if let error = response.result.error{
                        failure(error)
                    }
                    return
                }
                print(" ---------- MODEL  ---------- \n",item)
                guard let statusCode = response.response?.statusCode,statusCode == 200 else{
                    if let errorMessage = responseDict["message"] as? String{
                        failure(errorMessage)
                    }
                    return
                }
                success(item)
            case .failure(let error):
                failure(error)
            }
        }.session.finishTasksAndInvalidate()
    }
    
    
}
