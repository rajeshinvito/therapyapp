//
//  Alert.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/26/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
   
     func ShowAlert(message : String){
        let alertController = UIAlertController(title: kAppTitle, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
     }
    
    func ShowAlertWithPop(message : String){
        let alertController = UIAlertController(title: kAppTitle, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (UIAlertAction) in
            self.popToBackVC()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func ShowAlertWithpush(message : String,id: String,StoryID: String){
        let alertController = UIAlertController(title: kAppTitle, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (UIAlertAction) in
            self.movetonextvc(id: id, storyBordid: StoryID)
        }
//        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
//            UIAlertAction in
//        }
        alertController.addAction(OKAction)
        //alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
//    func ShowAlertWithAbort(message : String){
//           let alertController = UIAlertController(title: kAppTitle, message: message, preferredStyle: .alert)
//           let OKAction = UIAlertAction(title: "OK", style: .default) { (UIAlertAction) in
//            self.movetonextdashbordfromAbort()
//           }
//           let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
//               UIAlertAction in
//           }
//           alertController.addAction(OKAction)
//           alertController.addAction(cancelAction)
//           self.present(alertController, animated: true, completion: nil)
//       }
//
    
}

