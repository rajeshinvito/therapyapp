//
//  kithelper.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/28/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import UIKit


 extension String {
        func numberOfSeconds() -> Int {
           var components: Array = self.components(separatedBy: ":")
           let hours = Int(components[0]) ?? 0
           let minutes = Int(components[1]) ?? 0
           let seconds = Int(components[2]) ?? 0
           return (hours * 3600) + (minutes * 60) + seconds
       }
    }
