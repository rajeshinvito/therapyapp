//
//  ApiUrls.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/26/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation

let url = "http://anaadlive.com/therapist_13march/android/"


let loginUrl = url + "sign-in"
let UpdateuserDetailsURL = url + "edit-profile"
let ChangePassUrl = url + "change-password"
let timeInOut = url + "time-in-time-out"
let TodayAppointmentsUrl = url + "today-appointments"
let AppointmentHistoryUrl = url + "appointment-history"
let StartSessionUrl = url + "session-start-end"
let ReportSessionUrl = url + "weekly-timesheet"

let MYSCHEDULEURL  = url + "my-schedule"
let AddScheduleURL  = url + "add-schedule"
