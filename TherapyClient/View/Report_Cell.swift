//
//  ReportsVC.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/30/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import UIKit

//MARK:- reported cell
class Report_Cell : UITableViewCell{
    
    @IBOutlet weak var editbtnref: UIButton!
    
    @IBOutlet weak var Availebilitylblref: UILabel!
    
    @IBOutlet weak var usernamelblref: UILabel!
}
