//
//  Todayappointmentcells.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/27/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import UIKit

//MARK:- respective Cell
class TodayAppointmentsDoneCell : UITableViewCell {
    
    @IBOutlet weak var UserNameLblref: UILabel!
    @IBOutlet weak var AreaLblref: UILabel!
    @IBOutlet weak var TimeLblref: UILabel!
    @IBOutlet weak var sesssionStatusbtnref: UIButton!
}

//MARK:- respective Cell
class TodayAppointmentscurrentCell : UITableViewCell {
    @IBOutlet weak var UserNameLblref: UILabel!
    @IBOutlet weak var AreaLblref: UILabel!
    @IBOutlet weak var TimeLblref: UILabel!
    @IBOutlet weak var sesssionStatusbtnref: UIButton!
}

//MARK:- respective Cell
class TodayAppointmentsOnqueCell : UITableViewCell {
    @IBOutlet weak var UserNameLblref: UILabel!
    @IBOutlet weak var AreaLblref: UILabel!
    @IBOutlet weak var TimeLblref: UILabel!
          
}
