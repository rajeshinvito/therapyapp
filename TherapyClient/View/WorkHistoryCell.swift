//
//  WorkHistoryCell.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/28/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import UIKit

//MARK:- respective Cell
class WorkHistoryCell : UITableViewCell {
    @IBOutlet weak var UserNameLblref: UILabel!
    @IBOutlet weak var TimeINLblref: UILabel!
    @IBOutlet weak var TimeOutLblref: UILabel!
    @IBOutlet weak var TotalTimeLblref: UILabel!
    @IBOutlet weak var sesssionViewbtnref: UIButton!
}
