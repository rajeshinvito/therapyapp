//
//  ReportModel.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/30/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class reportWeeksessionsModel : Codable,Mappable {
    var status = ""
    var datas = [String]()
    var data = [reportWeeksessionsdata]()
    
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map["status"]
        data <- map["data"]
        datas <- map["datas"]
        
    }

}


class reportWeeksessionsdata : Codable,Mappable {
    
    var booking_id = ""
    var booking_date = ""
    var user_id = ""
    var first_name = ""
    var last_name = ""
    var time_in = ""
    var time_out = ""
    var total_time = ""
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        booking_id <- map["booking_id"]
        booking_date <- map["booking_date"]
        user_id <- map["user_id"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        time_in <- map["time_in"]
        time_out <- map["time_out"]
        total_time <- map["total_time"]
    }
}
