//
//  MyScheduleModel.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/30/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class myScheduleModel : Codable,Mappable {
    var status = ""
    var datas = [String]()
    var data = [myScheduledata]()
    
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map["status"]
        data <- map["data"]
        datas <- map["datas"]
        
    }

}


class myScheduledata : Codable,Mappable {
    var schedule_id = ""
    var therapist_id = ""
    var date = ""
    var start_time = ""
    var end_time = ""
    var day_period = ""
    var date_created = ""
    var status = ""
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
         schedule_id <- map["schedule_id"]
         therapist_id <- map["therapist_id"]
         date <- map["date"]
         start_time <- map["start_time"]
         end_time <- map["end_time"]
         day_period <- map["day_period"]
         date_created <- map["date_created"]
         status <- map["status"]
    }
}
