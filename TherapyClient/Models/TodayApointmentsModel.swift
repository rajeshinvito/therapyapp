//
//  TodayApointmentsModel.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/27/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import ObjectMapper

class TodayAppointmentsModel : Codable,Mappable {
    var status = ""
    var data = TodayAppointmentsData()
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map["status"]
        data <- map["data"]
    }

}

class TodayAppointmentsData : Codable,Mappable {
    var done = [TodayAppointments]()
    var current = [TodayAppointments]()
    var upcoming = [TodayAppointments]()
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        done <- map["done"]
        current <- map["current"]
        upcoming <- map["upcoming"]
    }
}


class TodayAppointments : Codable,Mappable {
    
    var booking_id = ""
    var user_id = ""
    var name = ""
    var booking_time = ""
    var booking_date = ""
    var location = ""
    var address = ""
    var latitude = ""
    var longitude = ""
    var date_created = ""
    var therapist_id = ""
    var assign_status = ""
    var appointment_status = ""
    var status = ""
    var first_name = ""
    var last_name = ""
    required init?(map: Map) {}
       init() {}
    func mapping(map: Map) {
         booking_id  <- map["booking_id"]
         user_id  <- map["user_id"]
         name  <- map["name"]
         booking_time  <- map["booking_time"]
         booking_date  <- map["booking_date"]
         location  <- map["location"]
         address  <- map["address"]
         latitude  <- map["latitude"]
         longitude  <- map["longitude"]
         date_created  <- map["date_created"]
         therapist_id  <- map["therapist_id"]
         assign_status  <- map["assign_status"]
         appointment_status  <- map["appointment_status"]
         status  <- map["status"]
         first_name  <- map["first_name"]
         last_name  <- map["last_name"]
    }
}


class AppointmentsStatusChangeModel : Codable,Mappable {
    var status = ""
    var msg = ""
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map["status"]
        msg <- map["msg"]
    }

}
