//
//  Loginvc.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/26/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import ObjectMapper

class LoginModel:Codable,Mappable {
    var status  = ""
    var msg = ""
    var data = loginData()
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map ["status"]
        msg <- map ["msg"]
        data <- map ["data"]
        
    }
}

class loginData : Codable,Mappable{
    var user_id = ""
    var first_name = ""
    var email = ""
    var phone = ""
    var address = ""
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        user_id <- map ["user_id"]
        first_name <- map ["first_name"]
        email <- map ["email"]
        phone <- map ["phone"]
        address <- map ["address"]
    }
}


class TimeIN_Out : Codable,Mappable {
    var status = ""
    var msg = ""
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map["status"]
        msg <- map["msg"]
    }

}

class UpdateDeails : Codable,Mappable {
    var status = ""
    var msg = ""
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map["status"]
        msg <- map["msg"]
    }

}

class ChangePass : Codable,Mappable {
    var status = ""
    var msg = ""
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map["status"]
        msg <- map["msg"]
    }

}
