//
//  WorkHistoryModel.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/28/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class workHistoryAppointmentsModel : Codable,Mappable {
    var status = ""
    var totals = ""
    var data = workHistoryAppointmentsdata()
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map["status"]
        data <- map["data"]
    }

}
class workHistoryAppointmentsdata : Codable,Mappable {
    var rides = [workHistoryAppointmentssessions]()
    required init?(map: Map) {}
      init() {}
    func mapping(map: Map) {
        rides <- map["rides"]
    }
}
class workHistoryAppointmentssessions : Codable,Mappable {
    var booking_id = ""
    var user_id = ""
    var booking_time = ""
    var booking_date = ""
    var address = ""
    var first_name = ""
    var last_name = ""
    var time_in = ""
    var time_out = ""
    var total_time = ""
    var total_time_2 = ""
    required init?(map: Map) {}
         init() {}
       func mapping(map: Map) {
        booking_id <- map["booking_id"]
        user_id <- map["user_id"]
        booking_time <- map["booking_time"]
        booking_date <- map["booking_date"]
        address <- map["address"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        time_in <- map["time_in"]
        time_out <- map["time_out"]
        total_time <- map["total_time"]
       }
}
