
//
//  WorkHistoryVC.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/24/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
import UIKit

class WorkHistoryVC: UIViewController {
    
    //MARK:- Class properties
    let dateFormatter = DateFormatter()
    let locale = NSLocale.current
    var datePicker : UIDatePicker!
    var toolBar = UIToolbar()
    var picker  = UIDatePicker()
    var userID = UserDefaults.standard.string(forKey: "user_id") ?? ""
    var workHistoryArr = [workHistoryAppointmentssessions]()
    let date = Date().string(format: "yyyy-MM-dd")
    let time = Date().string(format: "hh:mma")
    let initdate = Date().string(format: "dd MMM yyyy")
    var totaltime = String()
    //MARK:- Class IBoutlets
    @IBOutlet weak var Datelblref: UILabel!
    @IBOutlet weak var Timelblref: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var TotalCalllblref: UILabel!
    @IBOutlet weak var lastTimelblref: UILabel!
    
    //MARK:- View Lyfe Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
         self.TotalCalllblref.text = "Total Calls : 0"
        self.lastTimelblref.text = "Total time : 0"
        self.Datelblref.text = "Date : " + initdate
        self.Timelblref.text = "Time : " + time
        //Api calling...
         self.WorkHistoryVCAppointmentsMethod(date: date)
    }
    
    //MARK:- button actions
    @IBAction func Backbtn(_ sender: Any) {
        self.popToBackVC()
    }
    
    @IBAction func calenderEnblebtnref(_ sender: Any) {
        datePicker = UIDatePicker.init()
        datePicker.backgroundColor = UIColor.white
        
        datePicker.autoresizingMask = .flexibleWidth
        datePicker.datePickerMode = .dateAndTime
        
        datePicker.addTarget(self, action: #selector(self.dateChanged(_:)), for: .valueChanged)
        datePicker.frame = CGRect(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(datePicker)
        toolBar = UIToolbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = .blackTranslucent
        toolBar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.onDoneButtonClick))]
        toolBar.sizeToFit()
        self.view.addSubview(toolBar)
    }
    
    @objc func dateChanged(_ sender: UIDatePicker?) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        self.Datelblref.text = "Date:" + formatter.string(from: datePicker.date)
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "hh:mm a"
        self.Timelblref.text = "Time:" + formatter2.string(from: datePicker.date)
        
        let formatter3 = DateFormatter()
            formatter3.dateFormat = "yyyy-MM-dd"
        let selecteddate = formatter3.string(from: datePicker.date)
        self.WorkHistoryVCAppointmentsMethod(date: selecteddate)
        //self.WorkHistoryVCAppointmentsMethod(date: "2020-03-17")

    }
    
    @objc func onDoneButtonClick() {
        toolBar.removeFromSuperview()
        datePicker.removeFromSuperview()
    }
    
    
    //
}
extension WorkHistoryVC {
    func WorkHistoryVCAppointmentsMethod(date: String){
        indicator.showActivityIndicator()
        let perams = [
            "therapist_id": userID,
            "date" :date
        ]
        NetworkManager.Apicalling(url: AppointmentHistoryUrl, paramaters: perams, httpMethodType: .post, success: { (response:workHistoryAppointmentsModel) in
            if response.status == "1" {
                indicator.hideActivityIndicator()
                self.workHistoryArr.removeAll()
                if response.data.rides.count > 0 {
                    for i in 0..<response.data.rides.count {
                        self.workHistoryArr.append(response.data.rides[i])
                    }
                }
                
                self.tableView.reloadData()
            }else {
                indicator.hideActivityIndicator()
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
}


extension WorkHistoryVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.workHistoryArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : WorkHistoryCell  = tableView.dequeueReusableCell(withIdentifier: "WorkHistoryCell", for: indexPath) as! WorkHistoryCell
        cell.UserNameLblref.text = workHistoryArr[indexPath.row].first_name + workHistoryArr[indexPath.row].last_name
        cell.TimeINLblref.text = "Start : " + workHistoryArr[indexPath.row].time_in
        cell.TimeOutLblref.text = "End : " + workHistoryArr[indexPath.row].time_out
        cell.TotalTimeLblref.text = "Total - " + workHistoryArr[indexPath.row].total_time
        
        self.TotalCalllblref.text = "Total Calls : \(self.workHistoryArr.count)"
         var addTime = Int()
        if indexPath.row == 0 {
            let addTime = findDateDiff(time1Str: workHistoryArr[indexPath.row].time_out, time2Str: workHistoryArr[indexPath.row].time_in)
            totaltime = addTime
        }else {
            addTime = findDateDiff2(time1Str: workHistoryArr[indexPath.row].time_out, time2Str: workHistoryArr[indexPath.row].time_in)
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        var dateInput: Date? = formatter.date(from: totaltime)
        dateInput = dateInput?.addingTimeInterval(TimeInterval(addTime))
        totaltime = formatter.string(from: dateInput!)
        
        self.lastTimelblref.text = "Total time : " + totaltime
        
        return cell
    }
    
    
    func findDateDiff(time1Str: String, time2Str: String) -> String {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = "hh:mm a"
        var returntime = String()
        guard let time1 = timeformatter.date(from: time1Str),
            let time2 = timeformatter.date(from: time2Str) else { return "" }
        let interval = time2.timeIntervalSince(time1)
        self.hmsFrom(seconds: Int(interval)) { hours, minutes, seconds in
            let hours = self.getStringFrom(seconds: hours)
            let minutes = self.getStringFrom(seconds: minutes)
            let seconds = self.getStringFrom(seconds: seconds)
            
          returntime = "\(hours):\(minutes):\(seconds)"
        }
        return returntime
    }
    func findDateDiff2(time1Str: String, time2Str: String) -> Int {
                 let timeformatter = DateFormatter()
           timeformatter.dateFormat = "hh:mm a"
           guard let time1 = timeformatter.date(from: time1Str),
           let time2 = timeformatter.date(from: time2Str) else { return 0 }
           let interval = time2.timeIntervalSince(time1)

         
        return Int(interval)
       }
    func hmsFrom(seconds: Int, completion: @escaping (_ hours: Int, _ minutes: Int, _ seconds: Int)->()) {
        
        completion(seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
        
    }
    func getStringFrom(seconds: Int) -> String {
        
        return seconds < 10 ? "0\(seconds)" : "\(seconds)"
    }
}
