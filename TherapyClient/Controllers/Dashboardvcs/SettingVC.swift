//
//  SettingVC.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/24/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit

class SettingVC: UIViewController {
    
    //MARK:- class Outlets
    @IBOutlet weak var Nametfref: UITextField!
    @IBOutlet weak var Emailtfref: UITextField!
    @IBOutlet weak var PhoneTfref: UITextField!
    @IBOutlet weak var AddressTfref: UITextField!
    
    //MARK:- class properties
    let user_Id =  UserDefaults.standard.string(forKey: "user_id") as? String ?? ""
    //MARK: -view lyfe cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDetals()
    }
    
    //MARK:- button Actions
    //Back Btn
    @IBAction func Backbtn(_ sender: Any) {
        self.popToBackVC()
    }
    //Update btn
    @IBAction func Updatebtnref(_ sender: Any) {
        // TODO: - 👊 Validations -
        switch validateCredentials {
        case .valid:
            // self.moveToNext()
            self.UpdateDetails()
        case .invalid(let message):
            self.ShowAlert(message: message)
        }
    }
    //change password
    @IBAction func ChangePasswardbtnref(_ sender: Any) {
        //ChangePassVC
        self.movetonextvc(id:"ChangePassVC",storyBordid : "Main")
        
    }
    //Logout btnref
    @IBAction func Logoutbtnref(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "LoggedIn")
        UserDefaults.standard.removeObject(forKey: "user_id")
        UserDefaults.standard.removeObject(forKey: "first_name")
        UserDefaults.standard.removeObject(forKey: "email")
        UserDefaults.standard.removeObject(forKey: "phone")
        UserDefaults.standard.removeObject(forKey: "address")
        UserDefaults.standard.synchronize()
        //LoginVC
        self.movetonextvc(id:"LoginVC",storyBordid : "Main")
        
    }
}
extension SettingVC {
    
    func getUserDetals(){
        if let name = UserDefaults.standard.string(forKey: "first_name") as? String{
            self.Nametfref.text = name
        }
        if let email = UserDefaults.standard.string(forKey: "email") as? String{
            self.Emailtfref.text = email
        }
        if let phone = UserDefaults.standard.string(forKey: "phone") as? String{
            self.PhoneTfref.text = phone
        }
        if let address = UserDefaults.standard.string(forKey: "address") as? String{
            self.AddressTfref.text = address
        }
        
    }
    
    func UpdateDetails(){
        indicator.showActivityIndicator()
        let perams = [
            "therapist_id": user_Id,
            "email": self.Emailtfref.text ?? "",
            "phone":self.PhoneTfref.text ?? "",
            "address":self.AddressTfref.text ?? "",
            "name" : self.Nametfref.text ?? ""
        ]
        
        NetworkManager.Apicalling(url: UpdateuserDetailsURL, paramaters: perams, httpMethodType: .post, success: { (response:UpdateDeails) in
            if response.status == "1" {
                indicator.hideActivityIndicator()
                UserDefaults.standard.set(self.Nametfref.text ?? "", forKey: "first_name")
                UserDefaults.standard.set(self.Emailtfref.text ?? "", forKey: "email")
                UserDefaults.standard.set(self.PhoneTfref.text ?? "", forKey: "phone")
                UserDefaults.standard.set(self.AddressTfref.text ?? "", forKey: "address")
                self.ShowAlert(message: response.msg)
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.msg)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    var  validateCredentials : UserValidationState{
        if self.Nametfref.text ?? "" == ""  || self.Emailtfref.text ?? "" == "" || self.PhoneTfref.text ?? "" == "" || self.AddressTfref.text ?? "" == ""{
            return .invalid(ATErrorMessage.update.required)
        }else if self.Emailtfref.text ?? "" == "" {
            return .invalid(ATErrorMessage.update.email)
        }else if self.PhoneTfref.text ?? "" == "" {
            return .invalid(ATErrorMessage.update.Phone)
        }else if self.AddressTfref.text ?? "" == "" {
            return .invalid(ATErrorMessage.update.Address)
            
        }else if self.PhoneTfref.text?.count ?? 0 < 10 {
            return .invalid(ATErrorMessage.update.valid_Phone)
        }
        return .valid
        
    }
    
}
