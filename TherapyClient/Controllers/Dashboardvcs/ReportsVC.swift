
//
//  ReportsVC.swift
//  TherapyClient
//  Created by rajesh gandru on 3/24/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.

import UIKit

class ReportsVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    //MARK:- class properties
    var userID = UserDefaults.standard.string(forKey: "user_id") ?? ""
    var weekreportsArr = [reportWeeksessionsdata]()
    //MARK:- class outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var StartDatetfref: UITextField!
    @IBOutlet weak var EndDatetfref: UITextField!

    
    //MARK:- view lyfe cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //Api Calling....
        self.reportApiMethod()
        // Do any additional setup after loading the view.
        
        self.StartDatetfref.setInputViewDatePicker(target: self, selector: #selector(tapDone)) //1
        self.EndDatetfref.setInputViewDatePicker(target: self, selector: #selector(tapDone2)) //1

    }
    
    //MARK:- button actions
    @IBAction func Backbtn(_ sender: Any) {
         self.popToBackVC()
     }
    @IBAction func addSchedulebtnref(_ sender: Any) {
        //addScheduleVC
      self.movetonextvc(id:"addScheduleVC",storyBordid : "Main")
    }
    
    //MARK:- tableview
    //MARK:- tableview data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weekreportsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:Report_Cell = tableView.dequeueReusableCell(withIdentifier: "Report_Cell", for: indexPath) as! Report_Cell
        cell.Availebilitylblref.text = "My Availability : " + weekreportsArr[indexPath.row].booking_date
        cell.usernamelblref.text = weekreportsArr[indexPath.row].first_name + " " + weekreportsArr[indexPath.row].last_name + "\n\n" + "Start Time : " + weekreportsArr[indexPath.row].time_in + "\n" + "End Time : " + weekreportsArr[indexPath.row].time_out
        return cell
    }
    //MARK:- tableview delegates
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
           return 160.0
       }
    
    
    //2
    @objc func tapDone() {
        if let datePicker = self.StartDatetfref.inputView as? UIDatePicker { // 2-1
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateStyle = .medium // 2-3
            self.StartDatetfref.text = dateformatter.string(from: datePicker.date) //2-4
        }
        self.EndDatetfref.resignFirstResponder()
    }
    @objc func tapDone2() {
        if  let datePicker = self.EndDatetfref.inputView as? UIDatePicker { // 2-1
                   let dateformatter = DateFormatter() // 2-2
                   dateformatter.dateStyle = .medium // 2-3
                   self.EndDatetfref.text = dateformatter.string(from: datePicker.date) //2-4
               }
        self.EndDatetfref.resignFirstResponder()
    }
    
}

extension ReportsVC {
    //MARK:-   -- Api hitting -- 
    func reportApiMethod(){
        indicator.showActivityIndicator()
        let perams = [
            "therapist_id": "11",
            "start_date" : "2020-03-15",
            "end_date" :"2020-03-20"
        ]
        NetworkManager.Apicalling(url: ReportSessionUrl, paramaters: perams, httpMethodType: .post, success: { (response:reportWeeksessionsModel) in
            if response.status == "1" {
                indicator.hideActivityIndicator()
                if response.data.count > 0 {
                    for i in 0..<response.data.count {
                        self.weekreportsArr.append(response.data[i])
                    }
                }
                self.tableView.reloadData()
            }else {
                indicator.hideActivityIndicator()
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
}
