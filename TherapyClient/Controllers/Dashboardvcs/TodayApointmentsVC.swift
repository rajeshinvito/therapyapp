//
//  TodayApointmentsVC.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/24/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit

class TodayApointmentsVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK:- Class properties
    var TodayAppointmentsArr = [TodayAppointments]()
    var NextQueArr = [TodayAppointments]()
    
    var apointmentsarr = Array<Any>()
    var userID = UserDefaults.standard.string(forKey: "user_id") ?? ""
    
    //MARK:- class IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        apointmentsarr = ["Today Appointments","Next in queue"]
        
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
          //Api calling...
        self.TodayAppointmentsMethod()
    }
    //MARK:- Button Actions
    @IBAction func Backbtn(_ sender: Any) {
        self.popToBackVC()
    }
    //MARK:- Tableview implimentation
    
    
    //MARK:- Tableview Data Source
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return apointmentsarr[section] as? String
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return TodayAppointmentsArr.count
        }else {
            return NextQueArr.count
        }
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            if TodayAppointmentsArr[indexPath.row].appointment_status == "0" {
                let cell :TodayAppointmentscurrentCell = tableView.dequeueReusableCell(withIdentifier: "TodayAppointmentscurrentCell", for: indexPath) as! TodayAppointmentscurrentCell
                cell.UserNameLblref.text = TodayAppointmentsArr[indexPath.row].first_name + TodayAppointmentsArr[indexPath.row].last_name
                cell.AreaLblref.text = "Area : " + TodayAppointmentsArr[indexPath.row].address
                cell.TimeLblref.text = "Time : " + TodayAppointmentsArr[indexPath.row].booking_time
                cell.sesssionStatusbtnref.tag = indexPath.row
                cell.sesssionStatusbtnref.addTarget(self, action: #selector(startbtnref), for: .touchUpInside)
                return cell
            }else {
                let cell :TodayAppointmentsDoneCell = tableView.dequeueReusableCell(withIdentifier: "TodayAppointmentsDoneCell", for: indexPath) as! TodayAppointmentsDoneCell
                cell.UserNameLblref.text = TodayAppointmentsArr[indexPath.row].first_name + TodayAppointmentsArr[indexPath.row].last_name
                cell.AreaLblref.text = "Area : " + TodayAppointmentsArr[indexPath.row].address
                cell.TimeLblref.text = "Time : " + TodayAppointmentsArr[indexPath.row].booking_time
                if TodayAppointmentsArr[indexPath.row].appointment_status == "1" {
                    //session started
                    cell.sesssionStatusbtnref.setTitle("Started", for: .normal)
                }else if TodayAppointmentsArr[indexPath.row].appointment_status == "2" {
                    //session completed
                    cell.sesssionStatusbtnref.setTitle("Completed", for: .normal)

                }else if TodayAppointmentsArr[indexPath.row].appointment_status == "3" {
                    //session canceled
                    cell.sesssionStatusbtnref.setTitle("Canceled", for: .normal)

                }
                cell.sesssionStatusbtnref.tag = indexPath.row
                cell.sesssionStatusbtnref.addTarget(self, action: #selector(startbtnref), for: .touchUpInside)
                return cell

            }
        }else {
            let cell :TodayAppointmentsOnqueCell = tableView.dequeueReusableCell(withIdentifier: "TodayAppointmentsOnqueCell", for: indexPath) as! TodayAppointmentsOnqueCell
            cell.UserNameLblref.text = NextQueArr[indexPath.row].first_name + NextQueArr[indexPath.row].last_name
            cell.AreaLblref.text = "Area : " + NextQueArr[indexPath.row].address
            cell.TimeLblref.text = "Time : " + NextQueArr[indexPath.row].booking_time
            return cell
        }
     

    }

    //MARK:-Tableview Delegate
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160.0
    }
    @objc func startbtnref(_ sender : UIButton){
        let Storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nxtVC = Storyboard.instantiateViewController(withIdentifier: "sessionPreviewVC") as! sessionPreviewVC
        nxtVC.TodayAppointmentsArr = TodayAppointmentsArr[sender.tag]
        self.navigationController?.pushViewController(nxtVC, animated: true)
    }
    
}





extension TodayApointmentsVC {
    func TodayAppointmentsMethod(){
        indicator.showActivityIndicator()
        let perams = [
            "therapist_id":userID,
        ]
        NetworkManager.Apicalling(url: TodayAppointmentsUrl, paramaters: perams, httpMethodType: .post, success: { (response:TodayAppointmentsModel) in
            if response.status == "1" {
                indicator.hideActivityIndicator()
                if response.data.done.count > 0 {
                for i in 0..<response.data.done.count {
                    self.TodayAppointmentsArr.append(response.data.done[i])
                }
                }
                
                if response.data.current.count > 0 {
                    for i in 0..<response.data.current.count {
                        self.TodayAppointmentsArr.append(response.data.current[i])
                    }
                }
                if response.data.upcoming.count > 0 {
                    for i in 0..<response.data.upcoming.count {
                        self.NextQueArr.append(response.data.upcoming[i])
                    }
                }
                self.tableView.reloadData()
            }else {
                indicator.hideActivityIndicator()
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
}
