//
//  TimeInoutVC.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/24/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit

class TimeInoutVC: UIViewController {
    
    //MARK:- class properties
    var start_time = 0
    var timer_c = Timer()
    let user_id = UserDefaults.standard.string(forKey: "user_id") ?? ""
    //MARK:- Class IBoutlets
    @IBOutlet weak var timerlblref: UILabel!
    
    //MARK:- View Lyfe cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.timerlblref.text = ""
        if UserDefaults.standard.bool(forKey: "isstarted"){
            let seconds = UserDefaults.standard.integer(forKey: "start_sec")
            let time = Simple_Timer(seconds:  seconds , timer: self.timer_c, isTimerRunning: false, resumeTapped: false)
            time.runTimer()
            timer_c = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(TimeInoutVC.timerset)), userInfo: nil, repeats: true)
        }
    }
    
    
    //timeInOut
    //MARK:- Button actions
    @IBAction func Backbtn(_ sender: Any) {
        self.popToBackVC()
    }
    
    @IBAction func TimeInbtnref(_ sender: Any) {
        if  UserDefaults.standard.bool(forKey: "isstarted"){
            self.ShowAlert(message: "Your time slot already started.")
        }else{
            let timeIn =  UserDefaults.standard.string(forKey: "start_in")
            self.setTimer(timeIn: timeIn ?? "00:00:00",TimeOut : "00:00:00" ,timeAction : 1)
        }
        
    }
    
    
    @IBAction func TimeOutbntref(_ sender: Any) {
        if  UserDefaults.standard.bool(forKey: "isstarted"){
            let timeIn =  UserDefaults.standard.string(forKey: "start_in")
            self.setTimer(timeIn: "00:00:00",TimeOut : timeIn ?? "00:00:00" ,timeAction : 0)
        }else{
            self.ShowAlert(message: "please start time slot.")
        }
    }
    
    
    @objc func timerset(){
        let seconds = UserDefaults.standard.string(forKey: "start_time")
        self.timerlblref.text = seconds
    }
    
    
}

//MARK:- Api hitting
extension TimeInoutVC {
    //MARK:- login func
    func setTimer(timeIn: String,TimeOut : String ,timeAction : Int){
        UserDefaults.standard.removeObject(forKey: "Accesstoken")
        indicator.showActivityIndicator()
        let date = Date().string(format: "yyyy-MM-dd")
        let parameters = [
            "therapist_id":user_id,
            "current_date":date,
            "time_in":timeIn,
            "time_out":TimeOut,
            "time_action":timeAction
            ] as [String : Any]
        NetworkManager.Apicalling(url: timeInOut, paramaters: parameters, httpMethodType: .post, success: { (response:TimeIN_Out) in
            if response.status == "1" {
                if   UserDefaults.standard.bool(forKey: "isstarted"){
                    UserDefaults.standard.set(false, forKey: "isstarted")
                    let time = Simple_Timer(seconds: 0, timer: self.timer_c, isTimerRunning: false, resumeTapped: false)
                    time.pousetimer()
                    self.timerlblref.text = "00 sec"
                }else {
                    let time = Simple_Timer(seconds: 0, timer: self.timer_c, isTimerRunning: false, resumeTapped: false)
                    time.runTimer()
                    self.timer_c = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(TimeInoutVC.timerset)), userInfo: nil, repeats: true)
                    UserDefaults.standard.set(true, forKey: "isstarted")
                }
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.msg)
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.msg)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
}
