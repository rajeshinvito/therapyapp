//
//  Dashboardvc.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/24/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit

class Dashboardvc: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true

    }

    @IBAction func TodayApointmentsbtnref(_ sender: Any) {
        //TodayApointmentsVC
        self.movetonextvc(id:"TodayApointmentsVC",storyBordid : "Main")
    }
    
    
    @IBAction func TimeInoutbtnref(_ sender: Any) {
        //TimeInoutVC
        self.movetonextvc(id:"TimeInoutVC",storyBordid : "Main")

    }
    
    @IBAction func SubmitWorkDairybtnref(_ sender: Any) {
        //SubmitWorkDairyVC
        self.movetonextvc(id:"SubmitWorkDairyVC",storyBordid : "Main")

    }
    
    @IBAction func WorkHistorybtnref(_ sender: Any) {
       // WorkHistoryVC
        self.movetonextvc(id:"WorkHistoryVC",storyBordid : "Main")

    }
    
    @IBAction func Reportsbtnref(_ sender: Any) {
        //ReportsVC
        self.movetonextvc(id:"ReportsVC",storyBordid : "Main")

    }
    
    @IBAction func Settingbtnref(_ sender: Any) {
        //SettingVC
        self.movetonextvc(id:"SettingVC",storyBordid : "Main")

    }
    
    
    @IBAction func ALERTvcbtnref(_ sender: Any) {
        //AlertVC
        self.movetonextvc(id:"AlertVC",storyBordid : "Main")
    }
    
    @IBAction func messagevcbtnref(_ sender: Any) {
        //messagesvc
        self.movetonextvc(id:"messagesvc",storyBordid : "Main")
    }
}
