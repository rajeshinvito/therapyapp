//
//  webViewvc.swift
//  TherapyClient
//
//  Created by rajesh gandru on 4/4/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//


import UIKit
import WebKit

class webViewvc: UIViewController, WKNavigationDelegate, WKUIDelegate {
@IBOutlet weak var webviewref: WKWebView!
    var webView: WKWebView!
    var activityIndicator: UIActivityIndicatorView!

    override func viewDidLoad() {
        //webviewref = WKWebView(frame: CGRect.zero)
        webviewref.navigationDelegate = self
        webviewref.uiDelegate = self

       // view.addSubview(webView)

        activityIndicator = UIActivityIndicatorView()
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.gray

        view.addSubview(activityIndicator)

        webviewref.load(URLRequest(url: URL(string: "http://google.com")!))
    }

    func showActivityIndicator(show: Bool) {
        if show {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        showActivityIndicator(show: false)
    }

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        showActivityIndicator(show: true)
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        showActivityIndicator(show: false)
    }
    
    @IBAction func backviewref(_ sender: Any) {
              popToBackVC()
          }
}
