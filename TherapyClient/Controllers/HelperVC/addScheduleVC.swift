//
//  addScheduleVC.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/25/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.

import UIKit

class addScheduleVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    //MARK:- class properties
    var isselected = false
    let user_Id =  UserDefaults.standard.string(forKey: "user_id") ?? ""
    var mySchedulearr = [myScheduledata]()
    
    //MARK:- Class Outlets
    @IBOutlet weak var selectfulldatebtnref: UIButton!
    @IBOutlet weak var StartDatetfref: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var StartTimetfref: UITextField!
    
    @IBOutlet weak var EndTimeTfref: UITextField!
    
    //MARK:- view lyfe cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //Api calling...
        self.MyscheduleApiMethod()
        
        self.StartDatetfref.setInputViewDatePicker(target: self, selector: #selector(tapDone)) //1
        self.StartTimetfref.setInputViewDatePicker(target: self, selector: #selector(tapDone2)) //1
        self.EndTimeTfref.setInputViewDatePicker(target: self, selector: #selector(tapDone3)) //1

    }
    
    
    @IBAction func backbtnref(_ sender: Any) {
        self.popToBackVC()
    }
    @IBAction func fulldatebtnref(_ sender: Any) {
        if isselected == false {
            isselected = true
            self.selectfulldatebtnref.setImage(#imageLiteral(resourceName: "check"), for: .normal)
            
        }else{
            isselected = false
             self.selectfulldatebtnref.setImage(UIImage(named: "empty"), for: .normal)
        }

    }
    //2
     @objc func tapDone() {
         if let datePicker = self.StartDatetfref.inputView as? UIDatePicker { // 2-1
             let dateformatter = DateFormatter() // 2-2
             dateformatter.dateStyle = .medium // 2-3
             self.StartDatetfref.text = dateformatter.string(from: datePicker.date) //2-4
         }
         self.StartDatetfref.resignFirstResponder()
     }
     @objc func tapDone2() {
         if  let datePicker = self.StartTimetfref.inputView as? UIDatePicker { // 2-1
                    let dateformatter = DateFormatter() // 2-2
                    dateformatter.dateStyle = .medium // 2-3
                    self.StartTimetfref.text = dateformatter.string(from: datePicker.date) //2-4
                }
         self.StartTimetfref.resignFirstResponder()
     }
    @objc func tapDone3() {
           if  let datePicker = self.EndTimeTfref.inputView as? UIDatePicker { // 2-1
                      let dateformatter = DateFormatter() // 2-2
                      dateformatter.dateStyle = .medium // 2-3
                      self.EndTimeTfref.text = dateformatter.string(from: datePicker.date) //2-4
                  }
           self.EndTimeTfref.resignFirstResponder()
       }
    
    @IBAction func AddSchedulebtnref(_ sender: Any) {
        self.AddScheduleApiMethod()
    }
    
//MARK:- Tableview
    //MARK:- tableview data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mySchedulearr.count
    }
    
    //MARK:- tableview delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: addscadule_Cell = tableView.dequeueReusableCell(withIdentifier: "addscadule_Cell", for: indexPath) as! addscadule_Cell
        cell.namelblref.text = "Date : " + mySchedulearr[indexPath.row].date
        cell.Arealblref.text = "Start Time : " + mySchedulearr[indexPath.row].start_time
        cell.Timelblref.text = "End Time : "  + mySchedulearr[indexPath.row].end_time
        
        return cell
    }
}


extension addScheduleVC {
    //MARK:-   -- Api hitting -- 
    func MyscheduleApiMethod(){
        indicator.showActivityIndicator()
        let perams = [
            "therapist_id":"11",
            "current_date":"2020-03-18",
            "start_date":"2020-02-16",
            "end_date":"2020-03-21"
        ]
        NetworkManager.Apicalling(url: MYSCHEDULEURL, paramaters: perams, httpMethodType: .post, success: { (response:myScheduleModel) in
            if response.status == "1" {
                indicator.hideActivityIndicator()
                if response.data.count > 0 {
                    for i in 0..<response.data.count {
                    self.mySchedulearr.append(response.data[i])
                    }
                }
                self.tableView.reloadData()
            }else {
                indicator.hideActivityIndicator()
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    
    //MARK:-   -- Api hitting -- 
    func AddScheduleApiMethod(){
        indicator.showActivityIndicator()
        let perams = [
           "full_day":"1",
            "therapist_id":"11",
            "dates":"2020-03-21",
            "start_times":"04:01:00",
            "end_times":"05:30:00",
            "schedule_id":"1"
        ]
        NetworkManager.Apicalling(url: AddScheduleURL, paramaters: perams, httpMethodType: .post, success: { (response:myScheduleModel) in
            if response.status == "1" {
                indicator.hideActivityIndicator()
                if response.data.count > 0 {
                    for i in 0..<response.data.count {
                    self.mySchedulearr.append(response.data[i])
                    }
                }
                self.tableView.reloadData()
            }else {
                indicator.hideActivityIndicator()
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    
  
}
