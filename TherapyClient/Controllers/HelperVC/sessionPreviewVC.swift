//
//  sessionPreviewVC.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/27/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class sessionPreviewVC: UIViewController,CLLocationManagerDelegate,MKMapViewDelegate {
    
    //MARK:- class IBOutlets
    @IBOutlet weak var UserNamelblref: UILabel!
    @IBOutlet weak var AppointmentTimelblref: UILabel!
    @IBOutlet weak var AddressLblref: UILabel!
    @IBOutlet weak var Mobilelblref: UILabel!
    @IBOutlet weak var Startbtnref: UIButton!
    @IBOutlet weak var cancelledBtnref: UIButton!
    @IBOutlet weak var NoShowbtnref: UIButton!
    @IBOutlet weak var SessionEndbtnref: UIButton!
    @IBOutlet weak var MapViewref: MKMapView!
    @IBOutlet weak var MapViewbtnref: UIButton!
    @IBOutlet weak var Signaturebtnref: UIButton!
    @IBOutlet weak var OpenfromBtnref: UIButton!
    
    //MARK:- Class Properties
    var TodayAppointmentsArr = TodayAppointments()
    let locationManager = CLLocationManager()
    var selectedPin:MKPlacemark? = nil
    var placemarks = [MKMapItem]()

    var currentLoc = CLLocationCoordinate2D()
    var destinationLoc = CLLocationCoordinate2D()
    var islocation = false
    let date = Date().string(format: "yyyy-MM-dd")
    var userID = UserDefaults.standard.string(forKey: "user_id") ?? ""
    var isstarted = false
    
    var timer_c = Timer()

    //MARK:- view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locUpdate()
        // get desination location
        self.destinationLoc = CLLocationCoordinate2D(latitude: Double(TodayAppointmentsArr.latitude)!
            , longitude: Double(TodayAppointmentsArr.longitude)!)
        //Usre Name
        self.UserNamelblref.text = "Client Name : " + TodayAppointmentsArr.first_name +  TodayAppointmentsArr.last_name
        //address
        self.AddressLblref.text = "Address :"  + TodayAppointmentsArr.address
        //time in
        self.AppointmentTimelblref.text = "Appointment Time in : " + TodayAppointmentsArr.booking_time
        //mobilr
        self.Mobilelblref.text = "Mobile : " + TodayAppointmentsArr.last_name
        
        
        
        if TodayAppointmentsArr.appointment_status == "0" {
            self.Startbtnref.isHidden = false
            self.cancelledBtnref.isHidden = false
            self.SessionEndbtnref.isHidden = false
            self.NoShowbtnref.isHidden = false
            self.Signaturebtnref.isHidden = false
            self.Startbtnref.isUserInteractionEnabled = true
        }else  if TodayAppointmentsArr.appointment_status == "1"{
            self.Startbtnref.backgroundColor = .lightGray
            self.Startbtnref.isUserInteractionEnabled = false
            
        }else  if TodayAppointmentsArr.appointment_status == "2"{
            self.Startbtnref.isHidden = true
            self.cancelledBtnref.isHidden = true
            self.SessionEndbtnref.isHidden = true
            self.NoShowbtnref.isHidden = true
            self.Signaturebtnref.isHidden = true
        }else  if TodayAppointmentsArr.appointment_status == "3"{
            self.Startbtnref.isHidden = true
            self.cancelledBtnref.isHidden = true
            self.SessionEndbtnref.isHidden = true
            self.NoShowbtnref.isHidden = true
            self.Signaturebtnref.isHidden = true
        }else  if TodayAppointmentsArr.appointment_status == "4"{
            
        }
        
    }
    
//Button Action
    @IBAction func Backbtnref(_ sender: Any) {
        self.popToBackVC()
    }
    @IBAction func StartBtnref(_ sender: Any) {
      
            var timeIn = String()
            
            if  UserDefaults.standard.bool(forKey: "isstarted"){
                timeIn =  UserDefaults.standard.string(forKey: "start_in") ?? "00:00:00"
            }else {
                let time = Simple_Timer(seconds: 0, timer: self.timer_c, isTimerRunning: false, resumeTapped: false)
                time.runTimer()
                UserDefaults.standard.set(true, forKey: "isstarted")
            }
        let  perams = [
            "booking_id":TodayAppointmentsArr.booking_id,
            "today_date":date,
            "time_in":timeIn,
            "lat":TodayAppointmentsArr.latitude,
            "long":TodayAppointmentsArr.longitude,
            "therapist_id" : TodayAppointmentsArr.therapist_id,
            "for" : "1"
        ]
            self.sessionStartMethod(perams: perams)
        
    }
    
    @IBAction func SessionBtnref(_ sender: Any) {
       
            var timeOut = String()
            if  UserDefaults.standard.bool(forKey: "isstarted"){
                timeOut =  UserDefaults.standard.string(forKey: "start_in") ?? "00:00:00"
            }else {
                UserDefaults.standard.set(false, forKey: "isstarted")
                let time = Simple_Timer(seconds: 0, timer: self.timer_c, isTimerRunning: false, resumeTapped: false)
                time.pousetimer()
            }
     //My location
        let myLocation = CLLocation(latitude: currentLoc.latitude, longitude: currentLoc.longitude)

     //My buddy's location
        let myBuddysLocation = CLLocation(latitude: destinationLoc.latitude, longitude: destinationLoc.longitude)

     //Measuring my distance to my buddy's (in km)
     let distance = myLocation.distance(from: myBuddysLocation) / 1000


        let startedtime = UserDefaults.standard.string(forKey: "startedtime") ?? "00:00:00"
            let dateDiff = findDateDiff(time1Str: startedtime, time2Str: timeOut)
           let  perams = [
                "therapist_id":userID,
                "booking_id":TodayAppointmentsArr.booking_id,
                "today_date":date,
                "time_out":timeOut,
                "lat":TodayAppointmentsArr.latitude,
                "long":TodayAppointmentsArr.longitude,
                "total_time":dateDiff,
                "total_distance":distance,
                "for" : "2"
            ] as [String : Any]
        
        
        self.sessionStartMethod(perams: perams)
        
    }
    
    @IBAction func NoShowbtnref(_ sender: Any) {
        
        var timeIn = String()
        
        if  UserDefaults.standard.bool(forKey: "isstarted"){
            timeIn =  UserDefaults.standard.string(forKey: "start_in") ?? "00:00:00"
        }else {
            let time = Simple_Timer(seconds: 0, timer: self.timer_c, isTimerRunning: false, resumeTapped: false)
            time.runTimer()
            UserDefaults.standard.set(true, forKey: "isstarted")
        }
        let  perams = [
            "booking_id":TodayAppointmentsArr.booking_id,
            "today_date":date,
            "time_in":timeIn,
            "lat":TodayAppointmentsArr.latitude,
            "long":TodayAppointmentsArr.longitude,
            "therapist_id" : TodayAppointmentsArr.therapist_id,
            "for" : "4"
        ]
        self.sessionStartMethod(perams: perams)
    }
    
    @IBAction func cancelAppointment(_ sender: Any) {
        
        var timeIn = String()
        
        if  UserDefaults.standard.bool(forKey: "isstarted"){
            timeIn =  UserDefaults.standard.string(forKey: "start_in") ?? "00:00:00"
        }else {
            let time = Simple_Timer(seconds: 0, timer: self.timer_c, isTimerRunning: false, resumeTapped: false)
            time.runTimer()
            UserDefaults.standard.set(true, forKey: "isstarted")
        }
        let  perams = [
            "booking_id":TodayAppointmentsArr.booking_id,
            "today_date":date,
            "time_in":timeIn,
            "lat":TodayAppointmentsArr.latitude,
            "long":TodayAppointmentsArr.longitude,
            "therapist_id" : TodayAppointmentsArr.therapist_id,
            "for" : "3"
        ]
        self.sessionStartMethod(perams: perams)
    }
    
    @IBAction func SignatureBtnref(_ sender: Any) {
        //Signaturevc
        self.movetonextvc(id: "Signaturevc", storyBordid: "Main")
    }
    @IBAction func OpenFrobtnref(_ sender: Any) {
       // webViewvc
        self.movetonextvc(id: "webViewvc", storyBordid: "Main")
    }
    
    @IBAction func MapFullViewref(_ sender: UIButton){
        
        if let UrlNavigation = URL.init(string: "comgooglemaps://") {
            if UIApplication.shared.canOpenURL(UrlNavigation){
                if self.destinationLoc.longitude != nil && self.destinationLoc.latitude != nil {
                    
                    let lat =      self.destinationLoc.latitude
                    let longi = self.destinationLoc.longitude
                    
                    if let urlDestination = URL.init(string: "comgooglemaps://?saddr=&daddr=\(lat),\(longi)&directionsmode=driving") {
                        UIApplication.shared.openURL(urlDestination)
                    }
                }
            }
            else {
                NSLog("Can't use comgooglemaps://");
                self.openTrackerInBrowser()
                
            }
        }
        else
        {
            NSLog("Can't use comgooglemaps://");
            self.openTrackerInBrowser()
        }
        
        
        
    }
    func openTrackerInBrowser(){
        if self.destinationLoc.longitude != nil && self.destinationLoc.latitude != nil {
            
            let lat = self.destinationLoc.latitude
            let longi = self.destinationLoc.longitude
            
            if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=&daddr=\(lat),\(longi)&directionsmode=driving") {
                UIApplication.shared.openURL(urlDestination)
            }
        }
    }
    
}
extension sessionPreviewVC{
    func locUpdate(){
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }

        MapViewref.delegate = self
        MapViewref.mapType = .standard
        MapViewref.isZoomEnabled = true
        MapViewref.isScrollEnabled = true

        if let coor = MapViewref.userLocation.location?.coordinate{
            MapViewref.setCenter(coor, animated: true)
        }
    }
}


extension sessionPreviewVC  {
     func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
     func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            let span = MKCoordinateSpan.init(latitudeDelta: 0.05, longitudeDelta: 0.05)
            let region = MKCoordinateRegion(center: location.coordinate, span: span)
            self.currentLoc = location.coordinate
            if islocation == false {
                islocation = true
                self.showRouteOnMap(pickupCoordinate: self.currentLoc, destinationCoordinate: self.destinationLoc)
            }
            MapViewref.setRegion(region, animated: true)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: (error)")
    }
    
    @objc func getDirections(){
        if let selectedPin = selectedPin {
            let mapItem = MKMapItem(placemark: selectedPin)
            let launchOptions = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving]
            mapItem.openInMaps(launchOptions: launchOptions)
        }
    }
}
extension sessionPreviewVC   {

    // MARK: - showRouteOnMap

    func showRouteOnMap(pickupCoordinate: CLLocationCoordinate2D, destinationCoordinate: CLLocationCoordinate2D) {

        let sourcePlacemark = MKPlacemark(coordinate: pickupCoordinate, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinate, addressDictionary: nil)

        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)

        let sourceAnnotation = MKPointAnnotation()

        if let location = sourcePlacemark.location {
            sourceAnnotation.coordinate = location.coordinate
        }

        let destinationAnnotation = MKPointAnnotation()

        if let location = destinationPlacemark.location {
            destinationAnnotation.coordinate = location.coordinate
        }

        self.MapViewref.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )

        let directionRequest = MKDirections.Request()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile

        // Calculate the direction
        let directions = MKDirections(request: directionRequest)

        directions.calculate {
            (response, error) -> Void in

            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }

                return
            }

            let route = response.routes[0]

            self.MapViewref.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)

            let rect = route.polyline.boundingMapRect
            self.MapViewref.setRegion(MKCoordinateRegion(rect), animated: true)
            
            
        }
    }

    // MARK: - MKMapViewDelegate

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {

        let renderer = MKPolylineRenderer(overlay: overlay)

        renderer.strokeColor = UIColor(red: 17.0/255.0, green: 147.0/255.0, blue: 255.0/255.0, alpha: 1)

        renderer.lineWidth = 5.0

        return renderer
    }
}
extension sessionPreviewVC {
    func sessionStartMethod(perams : Dictionary<String,Any>){
        indicator.showActivityIndicator()
        NetworkManager.Apicalling(url: StartSessionUrl, paramaters: perams, httpMethodType: .post, success: { (response:AppointmentsStatusChangeModel) in
            if response.status == "1" {
                indicator.hideActivityIndicator()
                
                if self.TodayAppointmentsArr.appointment_status == "0"{
                    let timeOut =  UserDefaults.standard.string(forKey: "start_in") ?? "00:00:00"
                    UserDefaults.standard.set(timeOut, forKey: "startedtime")
                }
                
                if response.msg == "Your therapy session has been started." {
                    self.Startbtnref.backgroundColor = .lightGray
                    self.Startbtnref.isUserInteractionEnabled = false
                }else if response.msg == "Your therapy session is ended." {
                    self.SessionEndbtnref.backgroundColor = .lightGray
                    self.SessionEndbtnref.isUserInteractionEnabled = false
                }else if  response.msg == "Appointment cancelled."{
                    self.cancelledBtnref.backgroundColor = .lightGray
                    self.cancelledBtnref.isUserInteractionEnabled = false
                    self.SessionEndbtnref.backgroundColor = .lightGray
                    self.SessionEndbtnref.isUserInteractionEnabled = false
                    self.Startbtnref.backgroundColor = .lightGray
                    self.Startbtnref.isUserInteractionEnabled = false
                }else if response.msg == "Your request submitted successfully."{
                    self.cancelledBtnref.backgroundColor = .lightGray
                    self.cancelledBtnref.isUserInteractionEnabled = false
                    self.SessionEndbtnref.backgroundColor = .lightGray
                    self.SessionEndbtnref.isUserInteractionEnabled = false
                    self.Startbtnref.backgroundColor = .lightGray
                    self.Startbtnref.isUserInteractionEnabled = false
                    self.NoShowbtnref.backgroundColor = .lightGray
                    self.NoShowbtnref.isUserInteractionEnabled = false
                }
                self.ShowAlert(message: response.msg)
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.msg)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    
    func findDateDiff(time1Str: String, time2Str: String) -> String {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = "hh:mm:ss"

        guard let time1 = timeformatter.date(from: time1Str),
            let time2 = timeformatter.date(from: time2Str) else { return "" }

        //You can directly use from here if you have two dates

        let interval = time2.timeIntervalSince(time1)
        let hour = interval / 3600;
        let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
        let intervalInt = Int(interval)
        return "\(intervalInt < 0 ? "-" : "+") \(Int(hour)) Hours \(Int(minute)) Minutes"
    }
}
