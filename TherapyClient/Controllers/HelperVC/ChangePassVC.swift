//
//  ChangePassVC.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/27/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit

class ChangePassVC: UIViewController {
    //MARK:- class properties
    var userID = UserDefaults.standard.string(forKey: "user_id") as? String ?? ""
    //MARK:- class IBoutlets
    @IBOutlet weak var OldPasstfref: UITextField!
    @IBOutlet weak var NewTfref: UITextField!
    @IBOutlet weak var ConformTfref: UITextField!
    
    //View Lyfe cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
//MARK:- Button Action
    //Back btn
    @IBAction func Backbtnref(_ sender: Any) {
        self.popToBackVC()
    }
    //Submit btn
    @IBAction func Submitbtnref(_ sender: Any) {
        //MARK:- validation
        
        switch PasswardValidation {
        case .valid:
            changePass()
       case .invalid(let message):
       self.ShowAlert(message: message)
        }
    }
    

}
extension ChangePassVC {
    func changePass(){
        indicator.showActivityIndicator()
        let perams = [
            "therapist_id":userID,
            "old_password":OldPasstfref.text ?? "",
            "new_password":ConformTfref.text ?? ""
        ]
        
        NetworkManager.Apicalling(url: ChangePassUrl, paramaters: perams, httpMethodType: .post, success: { (response:ChangePass) in
            if response.status == "1" {
                self.OldPasstfref.text  =  ""
                self.ConformTfref.text = ""
                self.NewTfref.text =  ""
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.msg)
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.msg)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    var PasswardValidation : UserValidationState {
        if OldPasstfref.text ?? "" == "" || NewTfref.text ?? "" == "" || ConformTfref.text ?? "" == "" {
            return .invalid(ATErrorMessage.changePass.required)
        }else if OldPasstfref.text ?? "" == "" {
             return .invalid(ATErrorMessage.changePass.Oldpass)
        }else if NewTfref.text ?? "" == "" {
                         return .invalid(ATErrorMessage.changePass.newpass)
        }else if ConformTfref.text ?? "" == "" {
            return .invalid(ATErrorMessage.changePass.conform)

        }else if ConformTfref.text ?? "" != NewTfref.text ?? "" {
            return .invalid(ATErrorMessage.changePass.checking)
        }
        return .valid

    }
}
