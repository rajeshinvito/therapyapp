//
//  LoginVC.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/24/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit
class LoginVC: UIViewController {
    
    //MARK:- Class properties
    //MARK:- class outlets
    @IBOutlet weak var Emailtfref: UITextField!
    @IBOutlet weak var Passwordtfref: UITextField!
    
    //MARK:- view lyfe cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:- button actions
    @IBAction func signinbuttonref(_ sender: Any) {
        
        // TODO: - 👊 Validations -
        switch validateSignupCredentials {
        case .valid:
           // self.moveToNext()
            self.login()
        case .invalid(let message):
            self.ShowAlert(message: message)
        }
    }
    
    
}

//Api calling
extension LoginVC{
    
    //MARK:- login func
    func login(){
        indicator.showActivityIndicator()
          guard let email = Emailtfref.text else{
            indicator.hideActivityIndicator()
            return}
          guard let password = Passwordtfref.text else{
            indicator.hideActivityIndicator()
            return}
    
            let parameters = [
                "email":email,
                "password":password,
            ]
          NetworkManager.Apicalling(url: loginUrl, paramaters: parameters, httpMethodType: .post, success: { (response:LoginModel) in
            print(response.data)
            if response.status == "1" {
               indicator.hideActivityIndicator()
                UserDefaults.standard.set(true, forKey: "LoggedIn")
                UserDefaults.standard.set(response.data.user_id, forKey: "user_id")
                UserDefaults.standard.set(response.data.first_name, forKey: "first_name")
                UserDefaults.standard.set(response.data.email, forKey: "email")
                UserDefaults.standard.set(response.data.phone, forKey: "phone")
                UserDefaults.standard.set(response.data.address, forKey: "address")
                self.moveToNext()
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.msg)
            }
          }) { (errorMsg) in
              
              indicator.hideActivityIndicator()
              if let err = errorMsg as? String{
                self.ShowAlert(message: err)
              }
          }
      }
    
    //MARK:- move to next
    func moveToNext(){
       let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
       let newViewController = storyBoard.instantiateViewController(withIdentifier: "Dashboardvc") as! Dashboardvc
       let navigationController = UINavigationController(rootViewController: newViewController)
       let appdelegate = UIApplication.shared.delegate as! AppDelegate
       appdelegate.window!.rootViewController = navigationController
       }
}


//MARK: - Validation
extension LoginVC {
    var validateSignupCredentials:UserValidationState {
        if Emailtfref.text == "" || Passwordtfref.text ?? "" == "" {
            return .invalid(ATErrorMessage.login.required)
        }else if Emailtfref.text?.isEmpty == true  {
            return .invalid(ATErrorMessage.login.email)
        }else if Passwordtfref.text?.isEmpty == true {
            return .invalid(ATErrorMessage.login.password)
        }
//        else if Passwordtfref.text?.count ?? 0 < 8 {
//            return .invalid(ATErrorMessage.login.Maxpassword)
//        }
        return .valid
    }
    
    
}
